using OpenQA.Selenium;

public class GmailSettingsPage
{
    public string Url { get; }
    public string EditButton { get; }
    string UsernameField { get; }
    string GlobalEditField { get; }
    string SaveButton { get; }
    public GmailSettingsPage()
    {
        Url = "https://myaccount.google.com/personal-info";
        EditButton = @"div[jsname=""s3Eaab""]";
        UsernameField = @"input[type=""text""]";
        SaveButton = @"span[jsname=""V67aGc""]";
        GlobalEditField = @"div[class=""bJCr1d""]";
    }
    public IWebElement SearchUsernameField(IWebDriver driver)
    {
        return driver.FindElements(By.CssSelector(UsernameField))[0];
    }
    public IWebElement SearchGlobalEditField(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(GlobalEditField));
    }
    public IWebElement SearchEditButtonField(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(EditButton));
    }
    public IWebElement SearchSaveButton(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(SaveButton));
    }
}