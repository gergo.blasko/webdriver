using OpenQA.Selenium;

public class ProtonMailPage
{
    public string Inboxtext { get; }
    public string First_Unread_Email { get; }
    public string Url { get; }
    public string List_Of_Unread_Emails { get; }
    public string First_Email { get; }
    public string Empty_Mailbox { get; }
    public string Compose_Letter_Button { get; }
    public string Letter_Header { get; }
    public string Letter_Body { get; }
    public string Send_Letter { get; }
    public ProtonMailPage()
    {
        Inboxtext = @"h2[title=""Inbox""]";
        Url = "https://mail.proton.me/u/0/inbox#from=forepam1995%40gmail.com&filter=unread";
        List_Of_Unread_Emails = ".item-container-wrapper.relative";
        First_Email = @"#proton-root > div > div > div:nth-child(1)";
        Empty_Mailbox = @"mt-8";
        Compose_Letter_Button = @"button[data-testid=""sidebar:compose""]";
        Letter_Header = @"input[data-testid=""compose:to""]";
        Letter_Body = @"div[class="".ceditor-wrapper.fill.w100.h100.scroll-if-needed.flex-item-fluid.flex.flex-column.relative""]";
        Send_Letter = @"button[data-testid=""composer:send-button""]";
    }
    public IWebElement Search_For_Inbox_Text(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(Inboxtext));
    }
    public IWebElement Search_For_List_Of_Unread_Emails(IWebDriver driver)
    {
        return driver.FindElements(By.CssSelector(List_Of_Unread_Emails))[0];
    }
    public IWebElement Search_For_First_Email(IWebDriver driver)
    {
        return driver.FindElement(By.Id(First_Email));
    }
    public IWebElement Search_For_Empty_Mailbox_Sign(IWebDriver driver)
    {
        return driver.FindElement(By.ClassName(Empty_Mailbox));
    }
    public IWebElement Search_For_Compose_Letter_Button(IWebDriver driver)
    {
        return driver.FindElement(By.ClassName(Send_Letter));
    }
    public IWebElement Search_For_Letter_Header(IWebDriver driver)
    {
        return driver.FindElement(By.ClassName(Letter_Header));
    }
    public IWebElement Search_For_Letter_Body(IWebDriver driver)
    {
        return driver.FindElement(By.ClassName(Letter_Body));
    }
    public IWebElement Search_For_Send_Letter(IWebDriver driver)
    {
        return driver.FindElement(By.ClassName(Send_Letter));
    }
}