using OpenQA.Selenium;

public class ProtonMailLogin
{
    public string Url { get; }
    public string Username { get; }
    public string Password { get; }
    string UsernameField { get; }
    string PasswordField { get; }
    string NextButton { get; }
    public ProtonMailLogin()
    {
        Username = "forepam1995@protonmail.com";
        Password = "forepam1995";
        Url = "https://account.proton.me/mail";
        UsernameField = @"//*[@id=""username""]";
        PasswordField = @"//*[@id=""password""]";
        NextButton = @"/html/body/div[1]/div[4]/div[1]/main/div[1]/div[2]/form/button";
    }
    public IWebElement SearchUsernameField(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(UsernameField));
    }
    public IWebElement SearchPasswordField(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(PasswordField));
    }
    public IWebElement SearchNextButton(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(NextButton));
    }
}