using OpenQA.Selenium;

public class GmailPage
{
    public string Sender { get; }
    public string Receiver { get; }
    public string Receiverbody { get; }
    public string SendButton { get; }
    public string Url { get; }
    public GmailPage()
    {
        Sender = "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div/div";
        Receiver = @"input[role=""combobox""]";
        Receiverbody = @"div[role=""textbox""]";
        SendButton = @"div.T-I.J-J5-Ji.aoO.v7.T-I-atl.L3";
        Url = @"https://mail.google.com/mail/u/0/#inbox";
    }

    public IWebElement SearchForComposeEmailButton(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(Sender));
    }
    public IWebElement SearchForReceiver(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(Receiver));
    }
    public IWebElement SearchForReceiverbody(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(Receiverbody));
    }
    public IWebElement SearchForSendButton(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(SendButton));
    }
}