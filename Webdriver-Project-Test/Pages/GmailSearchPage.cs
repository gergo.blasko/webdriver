using OpenQA.Selenium;

public class GmailSearchPage
{
    public string Url { get; }
    public string Path_Of_First_Unread_Email { get; }
    public string First_Email_Value { get; }
    public GmailSearchPage()
    {
        Url = @"https://mail.google.com/mail/u/0/#search/label%3Aunread+forepam1995%40protonmail.com";
        Path_Of_First_Unread_Email = @"//*[@class='yW']/span";
        First_Email_Value = @"div[dir=""ltr""]";
    }

    public IWebElement SearchForFirstUnreadEmail(IWebDriver driver)
    {
        return driver.FindElements(By.XPath(Path_Of_First_Unread_Email))[0];
    }
    public IWebElement SearchForFirstUnreadEmailValue(IWebDriver driver)
    {
        return driver.FindElement(By.CssSelector(First_Email_Value));
    }
}