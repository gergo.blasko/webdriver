using OpenQA.Selenium;

public class GmailLogin_For_Username
{
    public string Url { get; }
    public string Username { get; }
    string UsernameField { get; }
    string NextButton { get; }
    public GmailLogin_For_Username()
    {
        Url = "https://accounts.google.com/v3/signin/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2Fu%2F0%2F&emr=1&followup=https%3A%2F%2Fmail.google.com%2Fmail%2Fu%2F0%2F&ifkv=AYZoVheAfOTrlg_ZNP-_S5OpsnKBaLPVcckopn43hA9SuLcvzD5OAwvtPeFzohAf3S7ENcRF2XUc5w&osid=1&passive=1209600&service=mail&flowName=GlifWebSignIn&flowEntry=ServiceLogin&dsh=S-1861445914%3A1696188914590142&theme=glif";
        Username = "forepam1995@gmail.com";
        UsernameField = @"//*[@id=""identifierId""]";
        NextButton = @"//*[@id=""identifierNext""]/div/button/span";
    }
    public IWebElement SearchUsernameField(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(UsernameField));
    }
    public IWebElement SearchNextButton(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(NextButton));
    }
}

public class GmailLogin_For_Password
{
    public string Password { get; }
    string PasswordField { get; }
    string NextButton { get; }
    public GmailLogin_For_Password()
    {
        Password = "forepam1995A!";
        PasswordField = @"//*[@id=""password""]/div[1]/div/div[1]/input";
        NextButton = @"//*[@id=""passwordNext""]/div/button";
    }
    public IWebElement SearchPasswordField(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(PasswordField));
    }
    public IWebElement SearchNextButton(IWebDriver driver)
    {
        return driver.FindElement(By.XPath(NextButton));
    }
}