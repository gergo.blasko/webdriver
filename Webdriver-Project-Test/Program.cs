﻿using NUnit.Framework;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

[TestFixture]
class TestClass
{
    GmailLogin_For_Username gmailLogin_For_Username;
    GmailLogin_For_Password gmailLogin_For_Password;
    GmailPage gmailPage;
    ProtonMailLogin protonMailLogin;
    ProtonMailPage protonMailPage;
    GmailSearchPage gmailSearchPage;
    GmailSettingsPage gmailSettingsPage;
    IWebDriver webDriver;

    [SetUp]
    public void Init()
    {
        webDriver = new ChromeDriver();
        webDriver.Manage().Window.Maximize();
    }

    [Test]
    public void Test_If_Login_To_Gmail_Is_Working()
    {
        // Arrange
        gmailLogin_For_Username = new GmailLogin_For_Username();
        gmailLogin_For_Password = new GmailLogin_For_Password();
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        // Act
        webDriver.Navigate().GoToUrl(gmailLogin_For_Username.Url);
        gmailLogin_For_Username.SearchUsernameField(webDriver).SendKeys(gmailLogin_For_Username.Username);
        gmailLogin_For_Username.SearchNextButton(webDriver).Click();
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        gmailLogin_For_Password.SearchPasswordField(webDriver).SendKeys(gmailLogin_For_Password.Password);
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        gmailLogin_For_Password.SearchNextButton(webDriver).Click();

        // Assert
        Assert.True(webDriver.PageSource.Contains("15 GB"));

    }

    [Test]
    public void Test_If_Login_To_Protonmail_Is_Working()
    {
        // Arrange
        protonMailLogin = new ProtonMailLogin();
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        // Act
        webDriver.Navigate().GoToUrl(protonMailLogin.Url);
        protonMailLogin.SearchUsernameField(webDriver).SendKeys(protonMailLogin.Username);
        protonMailLogin.SearchPasswordField(webDriver).SendKeys(protonMailLogin.Password);
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        protonMailLogin.SearchNextButton(webDriver).Click();

        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        // Assert
        Assert.Throws<NoSuchElementException>(() =>
         {
             var element = webDriver.FindElement(By.ClassName("mt-8"));
         });

    }

    [Test]
    public void Test_To_Send_Email_From_Gmail_Account_To_Protonmail_Account()
    {
        // Arrange
        gmailLogin_For_Username = new GmailLogin_For_Username();
        gmailLogin_For_Password = new GmailLogin_For_Password();
        gmailPage = new GmailPage();
        protonMailLogin = new ProtonMailLogin();
        protonMailPage = new ProtonMailPage();
        gmailSearchPage = new GmailSearchPage();
        gmailSettingsPage = new GmailSettingsPage();
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);


        // Act
        webDriver.Navigate().GoToUrl(gmailLogin_For_Username.Url);
        gmailLogin_For_Username.SearchUsernameField(webDriver).SendKeys(gmailLogin_For_Username.Username);
        gmailLogin_For_Username.SearchNextButton(webDriver).Click();
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        gmailLogin_For_Password.SearchPasswordField(webDriver).SendKeys(gmailLogin_For_Password.Password);
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        gmailLogin_For_Password.SearchNextButton(webDriver).Click();
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        gmailPage.SearchForComposeEmailButton(webDriver).Click();
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        gmailPage.SearchForReceiver(webDriver).SendKeys(protonMailLogin.Username);
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        gmailPage.SearchForReceiverbody(webDriver).SendKeys("TEST MESSAGE");
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        gmailPage.SearchForSendButton(webDriver).Click();

        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        webDriver.Navigate().GoToUrl(protonMailLogin.Url);
        webDriver.SwitchTo().Alert().Dismiss();
        webDriver.Navigate().GoToUrl(protonMailLogin.Url);
        webDriver.SwitchTo().Alert().Dismiss();
        try
        {
            protonMailLogin.SearchUsernameField(webDriver).SendKeys(protonMailLogin.Username);
        }
        catch (System.Exception)
        {

        }

        webDriver.Navigate().GoToUrl(protonMailLogin.Url);
        protonMailLogin.SearchUsernameField(webDriver).SendKeys(protonMailLogin.Username);
        protonMailLogin.SearchPasswordField(webDriver).SendKeys(protonMailLogin.Password);
        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        protonMailLogin.SearchNextButton(webDriver).Click();

        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        protonMailPage.Search_For_Inbox_Text(webDriver);

        webDriver.Navigate().GoToUrl(protonMailPage.Url);

        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

        protonMailPage.Search_For_List_Of_Unread_Emails(webDriver).Click();

        // Assert
        Assert.Throws<NoSuchElementException>(() =>
         {
             var element = protonMailPage.Search_For_Empty_Mailbox_Sign(webDriver);
         });

        webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

        Assert.That(protonMailPage.Search_For_First_Email(webDriver).Text == "TEST MESSAGE");

        var new_name = "AnonUser95";

        webDriver.Navigate().GoToUrl(protonMailLogin.Url);

        protonMailPage.Search_For_Compose_Letter_Button(webDriver).Click();
        protonMailPage.Search_For_Letter_Header(webDriver).SendKeys(gmailLogin_For_Username.Username);
        protonMailPage.Search_For_Letter_Body(webDriver).SendKeys(new_name);
        protonMailPage.Search_For_Send_Letter(webDriver).Click();

        webDriver.Navigate().GoToUrl(gmailSearchPage.Url);

        gmailSearchPage.SearchForFirstUnreadEmail(webDriver).Click();

        Assert.That(gmailSearchPage.SearchForFirstUnreadEmailValue(webDriver).Text == new_name);

        webDriver.Navigate().GoToUrl(gmailSettingsPage.Url);

        gmailSettingsPage.SearchGlobalEditField(webDriver).Click();

        gmailSettingsPage.SearchEditButtonField(webDriver).Click();

        gmailSettingsPage.SearchUsernameField(webDriver).SendKeys(new_name);

        gmailSettingsPage.SearchSaveButton(webDriver).Click();

    }
}